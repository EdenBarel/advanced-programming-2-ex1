﻿//eden barel
//313549024
//user name - bareled

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex1
{
    class ComposedMission : IMission
    {
        private List<Func<double, double>> funcs;
        private string name;
        //constructor
        public ComposedMission(string name)
        {
            this.funcs = new List<Func<double, double>>();
            this.name = name;
        }
        public string Name
        {
            get => name;
            private set => name = value;
        }


        //mission of "conposed" type.
        public string Type { get; private set; } = "Composed";

        public event EventHandler<double> OnCalculate;

        //adding new function to the function's list.
        public ComposedMission Add(Func<double, double> func)
        {
            funcs.Add(func);
            return this;
        }
    
        public double Calculate(double value)
        {
            double newValue = value;
            foreach(var f in funcs)
            {
                //the function operates over our new value.
                newValue = f(newValue);
            }
            //invoke the OnCalculate event
            OnCalculate?.Invoke(this, newValue);
            return newValue;
        }
    }
}
