﻿//eden barel
//313549024
//user name - bareled

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex1
{
    class FunctionsContainer
 
    {
        private Dictionary<string, Func<double, double>> funcDictionary;
        public FunctionsContainer()
        {
            this.funcDictionary = new Dictionary<string, Func<double, double>>();
        }
        public List<string> getAllMissions()
        {
            return funcDictionary.Keys.ToList();
        }

        public Func<double, double> this[string name]
        {
            get
            {
                //if the func exsists in the dictionary, we return the value.
                if (funcDictionary.ContainsKey(name))
                {
                    return this.funcDictionary[name];
                }
                else
                {
                    //otherwise- we add it and retuen the value with no changes.
                    funcDictionary.Add(name, value => value);
                    return value => value;
                }
            }
            set
            {
                //updating the dictionary.
                funcDictionary[name] = value;
            }
        }
    }
}
