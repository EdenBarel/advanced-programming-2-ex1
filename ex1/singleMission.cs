﻿//eden barel
//313549024
//user name - bareled

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex1
{
    public class SingleMission : IMission
    {
        private Func<double, double> func;
        private string name;
        //constructor.
        public SingleMission(Func <double,double> f, string s)
        {
            this.func = f;
            this.name = s;
        }
        public string Name
        {
            get => name;
            private set => name = value;
        }

        //mission of "single" type.
        public string Type { get; private set; } = "Single";

        public event EventHandler<double> OnCalculate;

        public double Calculate(double value)
        {
            //the function operates over our new value.
            double newValue = this.func(value);
            //invoke the OnCalculate event
            OnCalculate?.Invoke(this, newValue);
            return newValue;
        }
    }
}
